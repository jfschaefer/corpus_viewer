name := "rec_tree"

version := "1.0"

libraryDependencies ++= Seq(
  "org.scalafx" %% "scalafx" % "8.0.0-R4",
  "de.saar.coli" % "basics" % "1.2.34",
  "de.up.ling" % "irtg" % "1.1-SNAPSHOT",
  "net.sf.jung" % "jung2" % "2.0.1",
  "net.sf.jung" % "jung-visualization" % "2.0.1",
  "net.sf.jung" % "jung-algorithms" % "2.0.1",
  "net.sf.jung" % "jung-samples" % "2.0.1",
  "org.controlsfx" % "controlsfx" % "8.20.8",
  "de.up.ling" % "alto" % "1.1-SNAPSHOT",
  "de.up.ling" % "tuio-gesture" % "1.0.2-SNAPSHOT" //from "http://tcl.ling.uni-potsdam.de/artifactory/snapshots/de/up/ling/tuio-gesture/1.0.2-SNAPSHOT/tuio-gesture-1.0.2-20150629.074801-2.jar"
)

resolvers += "TCL Releases" at "http://tcl.ling.uni-potsdam.de/artifactory/release"

resolvers += "TCL Snapshots" at "http://tcl.ling.uni-potsdam.de/artifactory/snapshots"

resolvers += "TCL External" at "http://www.ling.uni-potsdam.de/tcl/maven2/external"
