package de.jfschaefer.corpus_viewer

import de.jfschaefer.corpus_viewer.graphfx._
import de.up.ling.irtg.algebra.graph.{GraphAlgebra, GraphNode, GraphEdge, SGraph}
import de.up.ling.tree.Tree

import scalafx.Includes._
import Util._

/**
 * Created by user on 6/23/15.
 */
trait CorpusEntry[EntryType, NodeType, EdgeType] {
  def getGraph() : GraphFx[NodeType, EdgeType]
}


class OurCorpusEntry /* [String, GraphNode, GraphEdge] */ (entry: java.lang.String) extends CorpusEntry[String, String, (String, String)] {
  override def getGraph() = {
    JungUtils.makeTree(de.up.ling.tree.TreeParser.parse(entry), (x: Tree[java.lang.String]) => Some(popOverFunction(x)), new TreeNodeConfiguration)
  }

  def popOverFunction(t:Tree[java.lang.String]) = {
    val sgraph = evaluateTree(t)
    val g = JungUtils.sgraphToJung(sgraph)
    val gprime = GraphFx.make(g, new GraphNodeConfiguration(sgraph), new GraphEdgeConfiguration, Main.graphDraggingEnabled)
    gprime.enableInteraction()
    gprime
  }

  def evaluateTree(t:Tree[java.lang.String]) = {
    val alg = new GraphAlgebra()
    alg.evaluate(t)
  }
}
