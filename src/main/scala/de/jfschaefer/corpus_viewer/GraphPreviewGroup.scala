package de.jfschaefer.corpus_viewer

import de.jfschaefer.corpus_viewer.graphfx.GraphFx
import de.jfschaefer.corpus_viewer.graphfx.Delta

import scalafx.beans.property.DoubleProperty
import scalafx.scene.{Group, Node}
import scalafx.scene.input.{ScrollEvent, MouseEvent}
import scalafx.Includes._

/**
 * Created by user on 6/24/15.
 */

object GraphPreviewGroup {
  // defined over [-1, 1], range [0, 1]  (more precisely [0.25, 1]
  def sizeFunction(x : Double) : Double = if (x < -1 || x > 1) 0.25 else 1-1.5 * x * x + 0.75 * x * x * x * x

  // defined over [-1, 1], range [0, 1]
  def distFunction(x : Double) : Double =
    /* integral over size function from -1 to x */ ((0.25 * x * (0.6 * x * x * x * x - 2 * x* x + 4)) + 0.65) /
    /* integral over size function from -1 to 1 */ 1.3

  //assuming x in (-2 + epsilon, 2 + epsilon)
  //won't be a bottle neck
  def distFunctionInverse(x : Double) : Double = secantMethod(-2, 2, distFunction(_) - x, 25)

  def secantMethod(a0 : Double, b0 : Double, f : Double => Double, nmax : Int) : Double = {
    val epsilon = 1e-12
    var a = a0
    var b = b0
    var n = 0
    assert(math.signum(f(a)) != math.signum(f(b)))
    while (n < nmax) {
      n = n + 1
      val c = 0.5 * (a + b)
      if (math.abs(f(c)) < epsilon) return epsilon
      if (math.signum(f(a)) == math.signum(f(c))) a = c else b = c
    }
    0.5 * (a + b)
  }
}

class GraphPreviewGroup[V, E](offset : DoubleProperty, getGraphClone: Int => GraphFx[V, E]) extends Group {
  var corpus: Seq[GraphFx[V, E]] = Nil
  val nodesInPreviewWrapper = 3
  val maxGraphHeight = 250
  val xOffset = 40 + Configuration.touchSliderWidth

  offset onChange {
    update()
  }

  def update(): Unit = {
    val centerIndex = math.floor(offset.value).toInt
    val reloffset = offset.value - centerIndex

    //Determine set of graphs in preview
    var graphsToBeDisplayed: List[(Double, GraphFx[V, E])] = Nil
    for (i <- -nodesInPreviewWrapper to nodesInPreviewWrapper) {
      val j = centerIndex - i
      if (j >= 0 && j < corpus.length) {
        graphsToBeDisplayed = (-i - reloffset, corpus(centerIndex - i)) :: graphsToBeDisplayed
      }
    }

    //update children
    var childrenToBeRemoved: List[Node] = Nil
    for (child <- children) {
      if (!graphsToBeDisplayed.contains(child) &&
             (draggedGraph != Some(child) || dragCurrentX - dragStartX < Configuration.previewMinDrag)) {
        childrenToBeRemoved ::= child
      }
    }
    for (child <- childrenToBeRemoved) {
      children.removeAll(child)
    }

    for ((pos, graph) <- graphsToBeDisplayed) {
      if (!children.contains(graph)) {
        children.add(graph)
      }
    }

    if (dragCurrentX - dragStartX >= Configuration.previewMinDrag) {
      draggedGraph match {
        case Some(graph) => if (!children.contains(graph)) children.add(graph)
        case None => {}
      }
    }

    //place children
    for ((pos, graph) <- graphsToBeDisplayed) {
      val npos = (pos - 0.5)/nodesInPreviewWrapper
      val scale = maxGraphHeight / (graph.boundsInLocal.value.getMaxY - graph.boundsInLocal.value.getMinY) * GraphPreviewGroup.sizeFunction(npos)
      graph.scaleX = scale
      graph.scaleY = scale
      graph.translateX = graph.translateX.value - graph.boundsInParent.value.getMinX + xOffset
      graph.translateY = graph.translateY.value - graph.boundsInParent.value.getMinY +
          GraphPreviewGroup.distFunction(npos) * Main.mainScene.getWindow.getHeight -
          0.5*(graph.boundsInParent.value.getMaxY - graph.boundsInParent.value.getMinY)
      associatedToDrag match {
        case Some(g) => {
          if (g == graph) {
            draggedGraph match {
              case Some(h) => {
                h.translateY = graph.translateY.value
                val ratio : Double = if (dragStartX < dragCurrentX)
                                       (dragCurrentX - dragStartX)/(Configuration.sizeOfPreviewSection - dragStartX)
                                     else 0d
                val scale : Double = if (Configuration.sizeOfPreviewSection < dragCurrentX) 1d else
                                        graph.scaleX.value + ratio * (1 - graph.scaleX.value)
                h.scaleX = scale
                h.scaleY = scale
                /*
                if (dragCurrentX > Configuration.sizeOfPreviewSection) { //Release graph
                  children.removeAll(h)
                  Main.mainScene.getChildren.add(h)
                  h.dragIsValid = true
                  //Sorry, GraphFx internally uses a different dragDelta convention
                  //layoutX.value - ev.sceneX / parent.value.scaleX.value
                  h.dragDelta.x = h.layoutX.value - dragCurrentX
                  h.dragDelta.y = h.layoutY.value - dragCurrentY
                  h.enableInteraction()
                  draggedGraph = None
                  associatedToDrag = None
                  dragIsValid = false
                  graph.previewDisabled.set(true)
                }
                */
                h.toFront()
              }
              case None => {
                System.out.println("de.jfschaefer.corpus_viewer.GraphPreviewGroup: Have associated graph, but no dragged graph")
              }
            }
          }
        }
        case None => {}
      }
    }
  }

  def yToRelPos(y: Double): Double = {
    val npos = GraphPreviewGroup.distFunctionInverse(y / Main.mainScene.getWindow.getHeight)
    val pos = npos * nodesInPreviewWrapper + 0.5
    pos
  }

  var dragDelta = Delta(0, 0)
  var dragStartPos = 0d

  var draggedGraph : Option[GraphFx[V, E]] = None
  var associatedToDrag : Option[GraphFx[V,E]] = None
  var dragIsValid = false
  var dragStartX = 0d
  var dragCurrentX = 0d
  var dragCurrentY = 0d

  onMousePressed = { ev: MouseEvent =>
    dragIsValid = true
    dragDelta = Delta(ev.sceneX, ev.sceneY)
    dragStartPos = yToRelPos(ev.sceneY)
    dragStartX = ev.sceneX
    dragCurrentX = ev.sceneX
    dragCurrentY = ev.sceneY
    val index = math.round(dragStartPos + offset.value).toInt
    if (0 <= index && index < corpus.length) {
      val graph = corpus(index)
      if (graph.boundsInParent.value.getMinX < ev.sceneX && ev.sceneX < graph.boundsInParent.value.getMaxX &&
        graph.boundsInParent.value.getMinY < ev.sceneY && ev.sceneY < graph.boundsInParent.value.getMaxY &&
        !graph.previewDisabled.value) {
        val newGraph = getGraphClone(index)
        children.add(newGraph)
        newGraph.scaleX = graph.scaleX.value
        newGraph.scaleY = graph.scaleY.value
        newGraph.translateX = graph.translateX.value
        newGraph.translateY = graph.translateY.value
        newGraph.isTrashed onChange { (a, b, c) =>
          if (newGraph.isTrashed.value) graph.previewDisabled.set(false)
        }
        draggedGraph = Some(newGraph)
        associatedToDrag = Some(graph)
      }
    }
  }

  onMouseDragged = {ev: MouseEvent =>
    dragCurrentX = ev.sceneX
    dragCurrentY = ev.sceneY
    if (dragIsValid) {
      val newPos = yToRelPos(ev.sceneY)
      draggedGraph match {
        case Some(graph) => {
          graph.translateX = graph.translateX.value + ev.sceneX - dragDelta.x
        }
        case None => {}
      }

      //if value doesn't change, onChange method won't be triggered
      if (offset.value == offset.value - newPos + dragStartPos) {
        update()
      } else {
        offset.set(offset.value - newPos + dragStartPos)  //includes update
      }
      dragStartPos = newPos
      dragDelta = Delta(ev.sceneX, ev.sceneY)
    }
  }

  onMouseReleased = {ev: MouseEvent =>
    if (dragCurrentX > Configuration.sizeOfPreviewSection) { //Release graph
      draggedGraph match {
        case None => {}
        case Some(h) => {
          children.removeAll(h)
          Main.mainScene.getChildren.add(h)
          h.dragIsValid = true
          //Sorry, GraphFx internally uses a different dragDelta convention
          //layoutX.value - ev.sceneX / parent.value.scaleX.value
          h.dragDelta.x = h.layoutX.value - dragCurrentX
          h.dragDelta.y = h.layoutY.value - dragCurrentY
          h.enableInteraction()
          associatedToDrag match {
            case None => {
              System.err.println("de.jfschaefer.corpus_viewer.GraphPreviewGroup: Dragged graph, but no associated graph set")
            }
            case Some(graph) => {
              graph.previewDisabled.set(true)
            }
          }
          draggedGraph = None
          associatedToDrag = None
          dragIsValid = false

        }
      }
    } else {
      dragIsValid = false
      draggedGraph match {
        case Some(graph) => {
          children.removeAll(graph)
        }
        case None => {}
      }
      draggedGraph = None
      associatedToDrag = None
    }
  }

  onScroll = { ev: ScrollEvent =>
    dragIsValid = false
    val newPos = yToRelPos(ev.sceneY)
    offset.set(offset.value - newPos + dragStartPos)
    draggedGraph = None
    associatedToDrag = None
    dragStartPos = newPos
  }
}
