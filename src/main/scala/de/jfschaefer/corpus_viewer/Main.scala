package de.jfschaefer.corpus_viewer

import scalafx.application.JFXApp
import scalafx.beans.property.BooleanProperty
import scalafx.scene.Scene
import scalafx.scene.Node
import scalafx.scene.control.Label
import scalafx.Includes._

import de.jfschaefer.corpus_viewer.graphfx._
import Util._

import de.up.ling.gesture.JavaFxAdapter
import de.up.ling.gesture.javafx.BoxDragEvent
import de.up.ling.irtg.algebra.graph.{GraphAlgebra, GraphNode, GraphEdge, SGraph}
import de.up.ling.tree.Tree

import scalafx.scene.image.{Image, ImageView}

object Main extends JFXApp {
  val corpusListing = new CorpusSearcher
  val graphDraggingEnabled = BooleanProperty(true)
  val mainScene = new Scene {
    stylesheets += "style.css"
    getChildren.add(Trash.getNode)
    Trash.trash = {n : Node => getChildren.removeAll(n)}
    getChildren.add(corpusListing)
  }

  stage = new JFXApp.PrimaryStage {
    title.value = Configuration.styleSheet
    width = 800
    height = 600

    scene = mainScene
  }

  val adapter = JavaFxAdapter.start(stage.delegate, "gesture-config.properties")
  adapter.addEventListener(BoxDragEvent.generateDragEvents)

  if (numScreens > Configuration.displayScreen) {
    fullscreen(stage, Configuration.displayScreen)
  } else {
    System.err.println("de.jfschaefer.corpus_viewer.Main: Couldn't display on screen " +
          Configuration.displayScreen + " (only have " + numScreens + " screens)")
  }


  Trash.placeTrashBottomLeft()
  corpusListing.placeLeft()
}
