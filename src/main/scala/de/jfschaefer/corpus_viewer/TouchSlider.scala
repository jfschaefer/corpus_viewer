package de.jfschaefer.corpus_viewer

import scalafx.scene.Group
import scalafx.Includes._
import scalafx.scene.shape.Rectangle
import scalafx.beans.property.DoubleProperty
import scalafx.scene.input.MouseEvent

/**
 * Created by user on 6/24/15.
 */

/*
   Why I don't use scalafx.scene.control.Slider:
        - Depending on how I build it, it doesn't appear for some reason
        - It's not easy to use it on a touch screen
        - Placing it properly and making sure it has the right size is a pain
 */

class TouchSlider extends Group {
  var rangeStart = DoubleProperty(0d)
  var rangeEnd = DoubleProperty(3d)
  var value = DoubleProperty(0d)

  val track = new Rectangle {
    width = Configuration.touchSliderWidth
    height = 200    //default height, supposed to be changed
    styleClass.add("touch_slider_track")
  }

  val rectangleThumbClip = new Rectangle {
    width = Configuration.touchSliderWidth
  }

  val thumb = new Rectangle {
    width = Configuration.touchSliderWidth
    height = 200
    styleClass.add("touch_slider_thumb")
    clip = rectangleThumbClip
  }


  thumb.layoutY onChange {
    val thumbBottom = thumb.layoutY.value + thumb.height.value
    val actualBottom = track.layoutY.value + track.height.value
    val thumbTop = thumb.layoutY.value
    val actualTop = track.layoutY.value
    rectangleThumbClip.layoutY  =  math.max(0, actualTop - thumbTop)
    rectangleThumbClip.height = math.min(thumb.height.value, thumb.height.value + actualBottom - thumbBottom)
  }

  children.add(track)
  children.add(thumb)

  updateThumbPos()

  track.heightProperty onChange updateThumbPos()
  rangeStart onChange updateThumbPos()
  rangeEnd onChange updateThumbPos()
  value onChange updateThumbPos()

  def updateThumbPos(): Unit = {
    assert(rangeStart.value < rangeEnd.value)
    thumb.layoutY = track.height.value / (rangeEnd.value - rangeStart.value) * (value.value - rangeStart.value) -
                    0.5 * thumb.height.value + track.layoutY.value
  }

  onMousePressed = { ev : MouseEvent =>
    updateValue(ev.y)
  }

  onMouseDragged = { ev: MouseEvent =>
    updateValue(ev.y)
  }

  def updateValue(y : Double) : Unit = {
    value.set((1-(track.height.value - track.layoutY.value - y)/track.height.value) * (rangeEnd.value - rangeStart.value))
  }
}
