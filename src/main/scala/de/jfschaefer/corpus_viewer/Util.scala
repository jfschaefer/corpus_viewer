package de.jfschaefer.corpus_viewer

import javafx.geometry.Bounds
import javafx.scene.shape.Rectangle

import de.up.ling.gesture.javafx.BoxDragEvent

import scalafx.geometry.Point2D
import scalafx.scene.paint.Color
import scalafx.scene.{Parent, Group, Scene, Node}

import scalafx.Includes._
import de.up.ling.tree.Tree

import scala.collection.{mutable, JavaConversions}
import scalafx.scene.input._
import scalafx.scene.text.Text
import scalafx.stage.{StageStyle, Screen, Stage}
import scala.collection.JavaConverters._

/**
 * Created by koller on 19.10.14.
 */

object Util {
  def pt(s:String) = de.up.ling.tree.TreeParser.parse(s)

  def foreach[E](collection: java.lang.Iterable[E], fn: E => Unit): Unit = {
    val it = collection.iterator()

    while( it.hasNext ) {
      fn(it.next())
    }
  }

  def map[E,V](collection: java.lang.Iterable[E], fn: E => V) : Seq[V] = {
    val ret = mutable.ArrayBuffer[V]()
    foreach(collection, { e:E => ret += fn(e) })
    ret
  }

  def dfs[E,V](tree:Tree[E], combine:(Tree[E],Seq[V]) => V) :  V = {
    val children = scala.collection.mutable.ArrayBuffer[V]()
    foreach(tree.getChildren, { ch : Tree[E] => children += dfs(ch, combine) })
    combine(tree, children)
  }

  def mapTree[I,O](tree:Tree[I], f:I => O) : Tree[O] = {
    dfs(tree, (subtree:Tree[I], children:Seq[Tree[O]]) => Tree.create(f(subtree.getLabel), children.asJava))
  }

  def foreachTopdown[E](tree:Tree[E], fn:E => Unit) : Unit = {
    fn(tree.getLabel)
    foreach(tree.getChildren, { ch : Tree[E] => foreachTopdown(ch, fn)})
  }

  def fullscreen(stage:Stage, displayId:Int): Unit = {
    val allScreens = Screen.screens
    val targetScreen = allScreens.get(displayId)
    val bounds = targetScreen.getVisualBounds

    stage.setX(bounds.getMinX())
    stage.setY(bounds.getMinY())
    stage.setWidth(bounds.getWidth())
    stage.setHeight(bounds.getHeight())

    stage.initStyle(StageStyle.UNDECORATED)
    stage.setFullScreen(true)
  }

  def numScreens : Int = Screen.screens.size

  def showDragBox(parent:Group) = new javafx.event.EventHandler[BoxDragEvent] {
    val boxDragRectangle = new Rectangle()

    def handle(event: BoxDragEvent) {
      if( event.getEventType == BoxDragEvent.BOXDRAG_STARTED ) {
          val box = event.getBounds
          val parentInScene = parent.localToScene(parent.boundsInLocal.value)

          boxDragRectangle.setX(box.getMinX - parentInScene.getMinX)
          boxDragRectangle.setY(box.getMinY - parentInScene.getMinY)
          boxDragRectangle.setWidth(box.getWidth)
          boxDragRectangle.setHeight(box.getHeight)
          boxDragRectangle.setFill(Color.rgb(250, 250, 250));
          boxDragRectangle.setStroke(Color.BLACK);
          boxDragRectangle.setStrokeWidth(1);

          parent.children.add(boxDragRectangle)
          boxDragRectangle.toBack
        } else if( event.getEventType == BoxDragEvent.BOXDRAG ) {
          boxDragRectangle.setX(boxDragRectangle.getX + event.getDeltaX)
          boxDragRectangle.setY(boxDragRectangle.getY + event.getDeltaY)
        } else if( event.getEventType == BoxDragEvent.BOXDRAG_FINISHED ) {
          parent.children.remove(boxDragRectangle)
        }
      }
  }

  def handleZoom(node:Node) = { ev:ZoomEvent => {
    val bip = node.boundsInLocal.value  //node.boundsInParent.value

    val localX = ev.x - bip.getMinX
    val localY = ev.y - bip.getMinY

    val shiftX = node.scaleX.value * (1 - ev.zoomFactor) * (localX - bip.getWidth/2)
    val shiftY = node.scaleY.value * (1 - ev.zoomFactor) * (localY - bip.getHeight/2)

    node.scaleX = node.scaleX.value * ev.zoomFactor
    node.scaleY = node.scaleY.value * ev.zoomFactor

    node.translateX = node.translateX.value + shiftX
    node.translateY = node.translateY.value + shiftY

    ev.consume
  }}

  def handleScroll(node:Node) = { ev:ScrollEvent => {
    node.translateX = node.translateX.value + ev.deltaX
    node.translateY = node.translateY.value + ev.deltaY
    ev.consume
  }}

  private val LONG_PRESS_DELAY = 500L

  def handleLongPress(node:Node)(startAction:Node => Unit, endAction:Node => Unit): Unit = {
    var startedPressing = Long.MaxValue
    var ignoreFurtherEvents = false

    node.onMousePressed = handle { ev:MouseEvent => {
      println(ev)

      if (!ignoreFurtherEvents) {
        val time = System.currentTimeMillis

        if (startedPressing == Long.MaxValue) {
          startedPressing = time
        } else {
          if (time >= startedPressing + LONG_PRESS_DELAY) {
            startAction(node)
            ignoreFurtherEvents = true
          }
        }
      }
    }}

    node.onTouchReleased = handle { ev:TouchEvent => {
      println(ev)

      endAction(node)
      startedPressing = Long.MaxValue
      ignoreFurtherEvents = false
    }}
  }

  def centerpoint(rect:Bounds) = new Point2D(rect.getMinX + rect.getWidth/2, rect.getMinY + rect.getHeight/2)
}
