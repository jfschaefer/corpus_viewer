package de.jfschaefer.corpus_viewer

import scalafx.scene.Group

/**
 * Created by user on 6/23/15.
 */

class CorpusSearcher extends Group {
  var sentences : Seq[CorpusEntry[String, String, (String, String)]] =
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    new OurCorpusEntry("'f_root:+'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:+'('merge:-'('f_1:+'('merge_root_1:-'('merge:+'('(u<root> :ARG1 (v<1>)):-','(u<root> / say-01):+'),'(u<root> / everything):-')),'(u<root> :manner (v<1>)):-'),'f_1:-'('merge_root_1:-'('merge:-'('(u<root> :mod (v<1>)):-','(u<root> / simple):-'),'(u<root> / magnificence):-')))),'(u<root> :ARG0 (v<1>)):-'),'(u<root> / king):-')))")::
    new OurCorpusEntry("f_subj(f_vcomp(merge(merge('(u<root> / want  :ARG0 (b<subj>)  :ARG1 (g<vcomp>))',r_subj('(x<root> / boy)')),r_vcomp('(g<root> / go  :ARG0 (s<subj>))'))))")::
    Nil    //they repeat, but that's the easiest way to simulate a larger corpus

  val getGraph = {id : Int => sentences(id).getGraph() }

  val coarseSlider = new TouchSlider

  children.add(coarseSlider)

  val preview = new GraphPreviewGroup[String, (String, String)](coarseSlider.value, getGraph) {
    corpus = sentences.map(_.getGraph())
  }

  children.add(preview)

  /* coarseSlider.value onChange {
    println(coarseSlider.value.value)
  } */

  def placeLeft(): Unit = {
    coarseSlider.setLayoutX(20)
    coarseSlider.setLayoutY(20)
    coarseSlider.track.height = coarseSlider.getScene.getWindow.getHeight - 40
    coarseSlider.rangeStart.set(0)
    coarseSlider.rangeEnd.set(sentences.length - 1)
    preview.update()
  }
}
