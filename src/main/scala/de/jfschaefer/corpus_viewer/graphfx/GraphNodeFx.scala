package de.jfschaefer.corpus_viewer.graphfx

import org.controlsfx.control.PopOver

import de.jfschaefer.corpus_viewer.Configuration
import de.jfschaefer.corpus_viewer.Main

import scala.annotation.tailrec
import scalafx.beans.binding.NumberBinding
import scalafx.beans.property.BooleanProperty
import scalafx.geometry.Point2D
import scalafx.scene.{Group, Node}
import scalafx.scene.control.Label
import scalafx.scene.input.MouseEvent
import scalafx.scene.shape.Rectangle

import scalafx.Includes._

/**
 * Created by koller on 01.11.14.
 */

class GraphNodeFx[V](label: V, nodeVisualizer: AbstractNodeConfiguration[V], graphState: GraphFxState,
                      sendExpansion : (Int, Int) => Unit = {(_, _) => Unit}) extends Group {
  val DRAG_THRESHOLD = Configuration.dragThreshold
  val MINIMAL_MOUSE_CLICK_DELAY = Configuration.minimalMouseClickDelay
                                        //Time in ms before the next onMouseClicked is accepted
                                        //Reason: Touch events tend to "flicker"
  var popover : Option[Node] = None
  val nodeState : GraphNodeFxState = new GraphNodeFxState

  private def checkMouseClickedTime : Boolean = {
    val cur = compat.Platform.currentTime
    if (cur - lastClickedTime >= MINIMAL_MOUSE_CLICK_DELAY) {
      lastClickedTime = cur
      true
    } else {
      lastClickedTime = cur   //Is this smart??
      //println("BLOCKED")
      false
    }
  }

  private val textNode : Label = new Label(nodeVisualizer.nodelabel(label)) {
    if (nodeVisualizer.interactionSettings.hasPopover) onMouseClicked = { ev: MouseEvent =>
      if (checkMouseClickedTime && !nodeState.hasBeenDragged && graphState.childInteractionEnabled) showOrHidePopover()
    }
  }

  private val bounds = nodeVisualizer.getLabelSize(label)
  private val delta = new Delta(0, 0) // for drag-and-drop
  private val dragStart = new Delta(0, 0)   //delta to corner ;-)
  private val dragStartLayout = new Delta(0, 0)
  private var lastClickedTime = 0l

  val rect : Rectangle = new Rectangle {
    width = math.max(bounds.getWidth, Configuration.nodeMinWidth) + 10
    height = bounds.getHeight + 10
    if (nodeVisualizer.interactionSettings.hasPopover) onMouseClicked = handle {
      if (checkMouseClickedTime && !nodeState.hasBeenDragged && graphState.childInteractionEnabled) showOrHidePopover()
    }
    minWidth(50)
  }

  def updateStyle() = {
    textNode.styleClass.clear()
    textNode.styleClass.add(nodeVisualizer.nodestyle(label, nodeState))
    rect.styleClass.clear()
    rect.styleClass.add(nodeVisualizer.nodestyle(label, nodeState))
  }

  textNode.layoutX <== rect.width / 2 - bounds.getWidth / 2
  textNode.layoutY <== rect.height / 2 - bounds.getHeight / 2

  updateStyle

  children = Seq(rect, textNode)

  def width = rect.width.value

  def height = rect.height.value

  def centerX: NumberBinding = layoutX + width / 2

  def centerY: NumberBinding = layoutY + height / 2

  private def topleft = new Point2D(layoutX.value, layoutY.value)

  def localMidpoint : Point2D = new Point2D(width/2, height/2)

  def midpoint : Point2D = localMidpoint.add(topleft)

  def findBoundaryPoint(a:Point2D) : Point2D = findBoundaryPoint(a.subtract(topleft), localMidpoint)

  @tailrec
  // a is outside, b is inside; all coordinates relative to GraphNodeFx
  private def findBoundaryPoint(a: Point2D, b: Point2D): Point2D = {
    val m = a.midpoint(b)

    if (a.distance(b) < 1) {
      m.add(topleft) // translate back to global coordinates
    } else {
      if( rect.contains(m) ) findBoundaryPoint(a, m) else findBoundaryPoint(m, b)
    }
  }

  private def showOrHidePopover() : Unit = {
    popover match {
      case Some(p) => {
        Main.mainScene.getChildren.removeAll(p)
        popover = None
        nodeState.setPopoverVisibity(false)
      }

      case None => {
        nodeVisualizer.popover(label) match {
          case Some(content) => {
            content.setLayoutX(layoutX.value * parent.value.scaleX.value + parent.value.getBoundsInParent.getMinX + parent.value.scaleX.value * rect.getWidth + 15)
            content.setLayoutY(layoutY.value * parent.value.scaleY.value + parent.value.getBoundsInParent.getMinY - 0.5*(
              content.getBoundsInParent.getMaxY - content.getBoundsInParent.getMinY
              ))
            Main.mainScene.getChildren.add(content)
            popover = Some(content)
            nodeState.setPopoverVisibity(true)
            content.isTrashed onChange { (a, b, c) =>
              if (content.isTrashed.value) {
                //is in trash
                nodeState.setPopoverVisibity(false)
                popover = None
                updateStyle()
              } else {   //won't be reached, but maybe we'll be able to recover things from trash
                nodeState.setPopoverVisibity(true)
                popover = Some(content)
                updateStyle()
              }
            }
          }
          case None => ;
        }

      }
    }
    updateStyle
  }

  def popoverToFront(): Unit = {
    popover match {
      case Some(p) => {
        p.toFront()
        nodeState.setPopoverVisibity(true)
      }
      case None => nodeState.setPopoverVisibity(false)
    }
    updateStyle
  }

  def removePopover(): Unit = {
    popover match {
      case Some(p) => Main.mainScene.getChildren.removeAll(p)
      case None => ;
    }
    nodeState.setPopoverVisibity(false)
  }

  if (nodeVisualizer.interactionSettings.draggable) {
    onMousePressed = { ev: MouseEvent => this.synchronized {
      if (graphState.childInteractionEnabled) {
        toFront()
        dragStart.x = ev.sceneX //for drags we don't scale, because we're only interested in the dragged screen distance
        dragStart.y = ev.sceneY
        dragStartLayout.x = layoutX.value
        dragStartLayout.y = layoutY.value
        delta.x = layoutX.value - ev.sceneX / parent.value.scaleX.value
        delta.y = layoutY.value - ev.sceneY / parent.value.scaleY.value

        nodeState.select()
        graphState.nodeIsBeingDragged_=(true)
        updateStyle
        ev.consume()
      }
    }
    }

    def aLessB2Int(a : Double, b : Double) : Int = {
      if (a < b) 1 else if (a > b) -1 else 0
    }

    onMouseDragged = { ev: MouseEvent =>
      if (nodeState.isSelected && graphState.childInteractionEnabled) this.synchronized {
        //avoid these annoying leaps when dragging over another node
        layoutX = ev.sceneX / parent.value.scaleX.value + delta.x
        layoutY = ev.sceneY / parent.value.scaleY.value + delta.y
        if ((dragStart.x - ev.sceneX) * (dragStart.x - ev.sceneX) +
          (dragStart.y - ev.sceneY) * (dragStart.y - ev.sceneY) > DRAG_THRESHOLD * DRAG_THRESHOLD) {
          nodeState.drag()
        }

        sendExpansion(aLessB2Int(parent.value.boundsInParent.value.getMaxX - parent.value.boundsInParent.value.getMinX,
          2 * centerX.value.doubleValue), //-1 if in the left half of parent, 1 if in the right one
          aLessB2Int(parent.value.boundsInParent.value.getMaxY - parent.value.boundsInParent.value.getMinY,
            2 * centerY.value.doubleValue))

        updateStyle
        ev.consume()
      }
      if (nodeState.isSelected && !graphState.childInteractionEnabled) {
        nodeState.unselect()
        graphState.nodeIsBeingDragged_=(false)
        sendExpansion(0, 0)
        //put node back to its original position
        layoutX = dragStartLayout.x
        layoutY = dragStartLayout.y
        updateStyle
      }
    }

    onMouseExited = { ev: MouseEvent =>   //for some reason onMouseDragExited doesn't work
      if (nodeState.isSelected && graphState.childInteractionEnabled) this.synchronized {
        //Remark: Doesn't completely fix the problem, just makes it a lot less likely
        layoutX = ev.sceneX / parent.value.scaleX.value + delta.x
        layoutY = ev.sceneY / parent.value.scaleY.value + delta.y
        //sendExpansion(aLessB2Int(dragStart.x, ev.sceneX), aLessB2Int(dragStart.y, ev.sceneY))
        sendExpansion(aLessB2Int(parent.value.boundsInParent.value.getMaxX + parent.value.boundsInParent.value.getMinX,
          2*centerX.value.doubleValue),  //-1 if in the left half of parent, 1 if in the right one
          aLessB2Int(parent.value.boundsInParent.value.getMaxY + parent.value.boundsInParent.value.getMinY,
            2*centerY.value.doubleValue))
        ev.consume()
      }
      updateStyle
    }

    onMouseDragExited = { ev: MouseEvent =>
      println("graphfx:GraphNodeFx: onMouseDragExited")   //Never happens, does it?
    }

    onMouseReleased = { ev: MouseEvent => this.synchronized {
      nodeState.unselect()
      graphState.nodeIsBeingDragged_=(false)
      sendExpansion(0, 0)
      updateStyle
      ev.consume()
    }
    }
  }
}
