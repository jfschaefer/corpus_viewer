package de.jfschaefer.corpus_viewer.graphfx

import javafx.geometry.{BoundingBox, Bounds}

import de.jfschaefer.corpus_viewer.Util._
import de.up.ling.tree.Tree

import scala.collection.{mutable, JavaConversions}
import scala.collection.mutable.ArrayBuffer
import scalafx.beans.property.ObjectProperty
import scalafx.geometry.Point2D
import scalafx.scene.control.Label
import scalafx.scene.paint.Color
import scalafx.scene.shape.Line
import scalafx.scene.text.Text
import scalafx.scene.{Group, Node, Scene}


/**
 * Created by koller on 28.10.14.
 */

object TreeFx {
  private final val XGAP: Int = 10
  private final val YLEVEL: Int = 50

  def make[E](tree:Tree[E], nodeVisualizer : AbstractNodeConfiguration[E] = new DefaultNodeConfiguration[E]()) : TreeFx[E] = {
    new TreeFx(layout(tree, nodeVisualizer), nodeVisualizer)
  }

  def layout[E](tree: Tree[E], vis:AbstractNodeConfiguration[E]): Tree[LabelAtPosition[E]] = {
    dfs(tree, (node: Tree[E], childrenValues: Seq[Tree[LabelAtPosition[E]]]) => {
      val labelSize = vis.getLabelSize(node.getLabel)

      // make room for surrounding rectangle
      val labelWidth = labelSize.getWidth + 10
      val labelHeight = labelSize.getHeight + 10

      var widthOfChildren: Double = 0
      var childrenMaxHeight : Double = 0

      // distribute children in X direction and measure maximum height of subtrees
      for (child <- childrenValues) {
        // distribute children in X direction
        shiftRight(child, widthOfChildren)

        // push children down to make room for root
        shiftDown(child, YLEVEL + labelHeight)

        // measure total width and height of subtrees
        widthOfChildren += child.getLabel.width + XGAP
        childrenMaxHeight = Math.max(childrenMaxHeight, child.getLabel.height)
      }

      if (widthOfChildren > 0) {
        widthOfChildren -= XGAP
      }

      // compute bounding box and text position for root
      val left : Double = 0
      val top : Double = 0
      val bottom = if( childrenValues.isEmpty ) labelHeight else childrenMaxHeight + YLEVEL + labelHeight
      val right : Double = Math.max(widthOfChildren, labelWidth)

      val textCenterX = if(childrenValues.isEmpty) {
        // leaf => text center is midpoint of label width
        right / 2
      } else if( labelWidth > widthOfChildren ) {
        // label is wider than all the children together => nudge children to the right
        val extraOffset = (labelWidth - widthOfChildren) / 2
        childrenValues.foreach { shiftRight(_, extraOffset) }
        right / 2
      } else {
        // position label at midpoint of the root labels of the children
        val leftOfLeftmostChildLabel: Double = childrenValues(0).getLabel.getTextRect.getMinX
        val rightOfRightmostChildLabel: Double = childrenValues(childrenValues.size - 1).getLabel.getTextRect.getMaxX
        (leftOfLeftmostChildLabel + rightOfRightmostChildLabel) / 2
      }

      // construct return object
      val label = LabelAtPosition(left, top, right, bottom, textCenterX, node.getLabel, labelSize, node)
      val javaChildren = JavaConversions.seqAsJavaList(childrenValues)
      Tree.create(label, javaChildren)
    })
  }


  private def shiftRight[E](tree: Tree[LabelAtPosition[E]], x: Double) {
    dfs(tree, (node:Tree[LabelAtPosition[E]], children:Seq[Unit]) => {
      node.getLabel.shiftRight(x)
    })
  }

  private def shiftDown[E](tree: Tree[LabelAtPosition[E]], y: Double) {
    dfs(tree, (node: Tree[LabelAtPosition[E]], children:Seq[Unit]) => {
      node.getLabel.shiftDown(y)
    })
  }

}

class TreeFx[E](tree:Tree[LabelAtPosition[E]], visualizer:AbstractNodeConfiguration[E]) extends Group {
  val lineColor = new ObjectProperty[Color]()
  lineColor.value = Color.BLACK

  lineColor.onChange {
    children = draw
  }


  val nodeVisualizer = new ObjectProperty[AbstractNodeConfiguration[E]]()
  nodeVisualizer.value = visualizer

  nodeVisualizer.onChange {
    children = draw
  }


  children = draw

  private def draw: Seq[Node] = {
    val allNodes = ArrayBuffer[Node]()

    dfs(tree, (node: Tree[LabelAtPosition[E]], childrenValues: Seq[Unit]) => {
      allNodes += makeLabel(node.getLabel)

      if (!childrenValues.isEmpty) {
        foreach(node.getChildren, { ch: Tree[LabelAtPosition[E]] =>
          allNodes += makeShortenedLine(node.getLabel, ch.getLabel)
        })
      }
    })

    allNodes
  }

  private def makeShortenedLine(parent:LabelAtPosition[E], child:LabelAtPosition[E]) : Node = {
    val line = new Line()

    val px = cx(parent.getTextRect)
    val py = cy(parent.getTextRect)
    val chx = cx(child.getTextRect)
    val chy = cy(child.getTextRect)

    val labelHeight = parent.labelSize.getHeight

    val diffx = chx - px
    val diffy = chy - py

    line.startX = px + 0.1*diffx
    line.startY = py + labelHeight

    line.endX = chx - 0.1*diffx
    line.endY = chy - labelHeight

    line.stroke = lineColor.value

    line
  }

  private def cx(rect:Bounds) = (rect.getMaxX + rect.getMinX) / 2
  private def cy(rect:Bounds) = (rect.getMaxY + rect.getMinY) / 2

  private def makeLabel(labelAtPos:LabelAtPosition[E]) : Node = {
    val label = new Label(nodeVisualizer.value.nodelabel(labelAtPos.label))
    val textRect = labelAtPos.getTextRect

    label.translateX = textRect.getMinX
    label.translateY = textRect.getMinY
    label.styleClass.add(nodeVisualizer.value.nodestyle(labelAtPos.label, new GraphNodeFxState))

    label
  }
}



case class LabelAtPosition[E](var left: Double, var top: Double, var right: Double, var bottom: Double, var textCenterX: Double, label:E, labelSize:Bounds, originalSubtree:Tree[E]) {
  def width = Math.abs(right - left)
  def height = Math.abs(top - bottom)

  def getTextRect = {
    new BoundingBox(left + textCenterX - labelSize.getWidth/2, top, labelSize.getWidth, labelSize.getHeight)
  }

  def shiftRight(x: Double) {
    left += x
    right += x
  }

  def shiftDown(y: Double) {
    top += y
    bottom += y
  }

  override def toString = {
    "[top=" + top + ", left=" + left + ", bottom=" + bottom + ", right=" + right + ", tcx=" + textCenterX + ", label=" + label + ']'
  }

}

class LabelAtPositionLayoutAdapter[V](positionTree:Tree[LabelAtPosition[V]]) extends AbstractNodeLayout[V] {
  private val nodeToPosition = new mutable.HashMap[V,Point2D]()

  // AbstractNodeLayout is supposed to return centerpoints of nodes => compute these
  foreachTopdown[LabelAtPosition[V]](positionTree, v => {
    nodeToPosition(v.label) = centerpoint(v.getTextRect)
  })

  override def position(node: V): Point2D = nodeToPosition(node)
}
