package de.jfschaefer.corpus_viewer.graphfx

/**
 * Created by user on 6/18/15.
 */

class GraphNodeFxState {  //State of a node; mainly for visualization purposes
  private var isSelected_ = false
  private var hasBeenDragged_ = false
  private var popoverVisible_ = false

  def isSelected : Boolean = isSelected_

  def select() = {
    isSelected_ = true
    hasBeenDragged_ = false
  }

  def unselect() = {
    isSelected_ = false
  }

  def popoverIsVisible : Boolean = popoverVisible_

  def setPopoverVisibity(b : Boolean): Unit = {
    popoverVisible_ = b
  }

  //returns true iff node has been dragged since it has been selected last
  def hasBeenDragged : Boolean = hasBeenDragged_

  def drag() = {
    if (isSelected) hasBeenDragged_ = true
    else {
      System.err.println("de.akoller.scalafx_test.graphfx.NodeState: Node is dragged, but not selected")
      isSelected_ = true
      hasBeenDragged_ = true
    }
  }
}
