package de.jfschaefer.corpus_viewer.graphfx

import de.jfschaefer.corpus_viewer.Util._
import de.jfschaefer.corpus_viewer.Main
import de.up.ling.irtg.algebra.graph.{GraphEdge, GraphNode, SGraph}
import de.up.ling.tree.Tree
import edu.uci.ics.jung.graph.{DirectedSparseGraph, DirectedGraph}
import scala.collection.JavaConverters._

import scalafx.scene.{Group, Node, Scene}
import scala.collection.mutable


/**
 * Created by koller on 01.11.14.
 */

object JungUtils {
  def makeTree[V](tree:Tree[V], nodeVisualizer : AbstractNodeConfiguration[V] = new DefaultNodeConfiguration[V]()) : GraphFx[String,(String,String)] = {
    val noPopovers = (x:Tree[V]) => None
    makeTree(tree, noPopovers, nodeVisualizer)
  }

  def makeTree[V](tree:Tree[V], popoverForSubtree:Tree[V] => Option[Node with Trashable], nodeVisualizer : AbstractNodeConfiguration[V] = new DefaultNodeConfiguration[V]()) : GraphFx[String,(String,String)] = {
    val map = new mutable.HashMap[String,Tree[V]]()
    val nameTree = JungUtils.nameTreeNodes(tree, map)

    val edgeVisualizer = new DefaultEdgeConfiguration[(String,String)]()
    val nv : AbstractNodeConfiguration[String] = new NodeLabelExtractingConfiguration(map.toMap, nodeVisualizer, popoverForSubtree)

    val layoutTree : Tree[LabelAtPosition[String]] = TreeFx.layout(nameTree, nv)
    val layout = new LabelAtPositionLayoutAdapter[String](layoutTree)

    val graph = JungUtils.treeToJung(nameTree)

    new GraphFx[String, (String,String)](graph, layout, nv, edgeVisualizer, Main.graphDraggingEnabled)
  }



  def sgraphToJung(sgraph:SGraph) : DirectedGraph[GraphNode,GraphEdge] = {
    val ret = new DirectedSparseGraph[GraphNode,GraphEdge]()

    val graph : org.jgrapht.DirectedGraph[GraphNode, GraphEdge] = sgraph.getGraph

    foreach[GraphNode](graph.vertexSet, v => { ret.addVertex(v) })
    foreach[GraphEdge](graph.edgeSet, e => { ret.addEdge(e, graph.getEdgeSource(e), graph.getEdgeTarget(e)) })

    ret
  }

  def nameTreeNodes[E](tree:Tree[E], nameToLabel:mutable.Map[String,Tree[E]]) : Tree[String] = {
    val gensym = new Gensym("u")

    dfs[E,Tree[String]](tree, (node, children) => {
      val name = gensym.gensym
      nameToLabel(name) = node
      Tree.create(name, children.asJava)
    })
  }

  def treeToJung[E](tree:Tree[E]) : DirectedGraph[E,(E,E)] = {
    val ret = new DirectedSparseGraph[E,(E,E)]()

    dfs[E,E](tree, (node, children) => {
      val lab = node.getLabel
      ret.addVertex(lab)
      children.foreach(ch => ret.addEdge((lab, ch), lab, ch))
      lab
    })
    
    ret
  }
}

class Gensym(prefix:String) {
  private var next = 1

  def gensym = {
    val ret = prefix + next
    next = next + 1
    ret
  }
}
