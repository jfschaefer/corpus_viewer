package de.jfschaefer.corpus_viewer.graphfx

import javafx.geometry.Bounds

import de.up.ling.tree.Tree

import scalafx.scene.{Node, Group, Scene}
import scalafx.scene.control.Label
import scalafx.scene.text.Text
import scalafx.beans.property.BooleanProperty

import de.up.ling.irtg.algebra.graph.{GraphNode, SGraph}

/**
 * Created by koller on 01.11.14.
 */

case class NodeInteractionSettings(draggable: Boolean, hasPopover: Boolean)

trait AbstractNodeConfiguration[E] {
  def nodelabel(node:E) : String
  def nodestyle(node:E, state:GraphNodeFxState) : String

  def popover(node:E) : Option[Node with Trashable] = None

  def getLabelSize(label: E): Bounds = {
    val text = new Text(nodelabel(label))
//    text.styleClass.add(nodestyle(label))  // this doesn't work because stylesheet is not set on Scene x
    val x = new Scene(new Group(text))
    text.getLayoutBounds
  }

  var interactionSettings : NodeInteractionSettings = NodeInteractionSettings(false, false)
}

class DefaultNodeConfiguration[E] extends AbstractNodeConfiguration[E] {
  override def nodelabel(node:E) = node.toString
  override def nodestyle(node:E, state:GraphNodeFxState) = "graphfx_node_default"
}

class NodeLabelExtractingConfiguration[K,V](map:Map[K,Tree[V]], underlyingVisualizer:AbstractNodeConfiguration[V],
                                            popoverForSubtree:Tree[V] => Option[Node with Trashable]) extends AbstractNodeConfiguration[K] {
  override def nodelabel(node: K): String = underlyingVisualizer.nodelabel(map(node).getLabel)
  override def nodestyle(node: K, state:GraphNodeFxState): String = underlyingVisualizer.nodestyle(map(node).getLabel, state)
  override def popover(node: K) : Option[Node with Trashable] = popoverForSubtree(map(node))
  interactionSettings = NodeInteractionSettings(false, true)
}

class GraphNodeConfiguration(sgraph:SGraph) extends AbstractNodeConfiguration[GraphNode] {
  override def nodelabel(node:GraphNode) = node.getLabel
  override def nodestyle(node:GraphNode, state:GraphNodeFxState) = if (sgraph.isSourceNode(node.getName)) "graphfx_node_source" else "graphfx_node_default"
  override def popover(node:GraphNode) = None // if( sgraph.isSourceNode(node.getName) ) Some(Label(sgraph.getSourcesAtNode(node.getName).toString)) else None
  interactionSettings = NodeInteractionSettings(true, false)
}

class TreeNodeConfiguration extends AbstractNodeConfiguration[String] {
  override def nodelabel(node:String) = node
  override def nodestyle(node:String, state:GraphNodeFxState) =
    if (state.isSelected) "tree_node_selected"
    else if (state.popoverIsVisible) "tree_node_with_popover"
    else "tree_node_default"
}
