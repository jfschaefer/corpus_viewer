package de.jfschaefer.corpus_viewer.graphfx

import scalafx.Includes._
import scalafx.scene.Node
import scalafx.scene.paint.Color
import scalafx.scene.shape.Polygon

import de.up.ling.irtg.algebra.graph.{GraphEdge}

/**
 * Created by koller on 01.11.14.
 */

trait AbstractEdgeConfiguration[E] {
  def edgelabel(edge:E) : Option[String]
  def edgestyle(edge:E) : String
  def arrowhead(edge:E) : Option[Node]
}

abstract class ArrowheadEdgeConfiguration[E] extends AbstractEdgeConfiguration[E] {
  override def arrowhead(edge:E) = {
    val c = Polygon(0,0,  -8,-4,  -8,4)
    c.fill = Color.BLACK
    c.stroke = Color.BLACK
    Some(c)
  }
}

class DefaultEdgeConfiguration[E] extends AbstractEdgeConfiguration[E] {
  override def edgelabel(edge: E): Option[String] = None
  override def arrowhead(edge: E): Option[Node] = None
  override def edgestyle(edge: E): String = "graphfx_edge_default"
}


class GraphEdgeConfiguration extends ArrowheadEdgeConfiguration[GraphEdge] {
  override def edgelabel(edge: GraphEdge): Option[String] = Some(edge.getLabel)
  override def edgestyle(edge: GraphEdge): String = "graphfx_edge_default"
}
