package de.jfschaefer.corpus_viewer.graphfx

import edu.uci.ics.jung.algorithms.layout.Layout

import scalafx.geometry.Point2D

/**
 * Created by koller on 01.11.14.
 */

trait AbstractNodeLayout[V] {
  def position(node:V) : Point2D
}

class JungLayoutAdapter[V,E](jungLayout:Layout[V,E]) extends AbstractNodeLayout[V] {
  override def position(node: V): Point2D = {
    val p = jungLayout.transform(node)
    new Point2D(p.getX, p.getY)
  }
}
