package de.jfschaefer.corpus_viewer.graphfx

import scalafx.Includes._
import scalafx.geometry.Point2D
import scalafx.scene.text.Text
import scalafx.scene.transform.{Affine, Transform, Rotate}
import scalafx.scene.{Node, Group}
import scalafx.scene.shape.Line

/**
 * Created by koller on 01.11.14.
 */

class GraphEdgeFx[V,E](label:E, pStart:GraphNodeFx[V], pEnd:GraphNodeFx[V], edgeVisualizer: AbstractEdgeConfiguration[E]) extends Group {
  val line = new Line()
  line.startX <== pStart.centerX
  line.startY <== pStart.centerY
  line.endX <== pEnd.centerX
  line.endY <== pEnd.centerY

  var startMidpoint : Point2D = null
  var endMidpoint : Point2D = null
  var lineAngle : Double = 0

  recomputeChildren

  pStart.layoutX.onChange {
    recomputeChildren
  }

  pStart.layoutY.onChange {
    recomputeChildren
  }

  pEnd.layoutX.onChange {
    recomputeChildren
  }

  pEnd.layoutY.onChange {
    recomputeChildren
  }

  private def recomputeChildren: Unit = {
    recomputeLineAngle
    children = Seq(Some(line), makeArrowhead, makeEdgeLabel).flatten
  }

  private def recomputeLineAngle: Unit = {
    startMidpoint = pStart.midpoint
    endMidpoint = pEnd.midpoint

    // angle to rotate horizontal line clockwise to u -> v
    val atan = Math.atan2(endMidpoint.y - startMidpoint.y, endMidpoint.x - startMidpoint.x)
    lineAngle = Math.toDegrees(atan)
  }

  private def makeArrowhead : Option[Node] = {
    val _c = edgeVisualizer.arrowhead(label)

    for( c <- _c ) yield {
      // move arrowhead to correct position
      val intersect = pEnd.findBoundaryPoint(pStart.midpoint)
      c.layoutX = intersect.x
      c.layoutY = intersect.y

      c.transforms.add(new Rotate(lineAngle, 0, 0))

      c
    }
  }

  private def makeEdgeLabel : Option[Node] = {
    for( edgelabel <- edgeVisualizer.edgelabel(label) ) yield {
      val node = new Text(edgelabel)

      node.layoutX = (startMidpoint.x + endMidpoint.x)/2
      node.layoutY = (startMidpoint.y + endMidpoint.y)/2

      node
    }
  }

  private def affine(mxx:Double, mxy:Double, tx:Double, myx:Double, myy:Double, ty:Double) : Transform = {
    val ret = new Affine()
    ret.mxx = mxx
    ret.mxy = mxy
    ret.tx = tx
    ret.myx = myx
    ret.myy = myy
    ret.ty = ty
    ret
  }
}
