package de.jfschaefer.corpus_viewer.graphfx

/**
 * Created by user on 6/23/15.
 */

case class Delta(var x: Double, var y: Double)
