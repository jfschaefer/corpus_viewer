package de.jfschaefer.corpus_viewer.graphfx


import de.jfschaefer.corpus_viewer.Util._
import de.jfschaefer.corpus_viewer.{Trash, Configuration}
import edu.uci.ics.jung.graph.Graph
import edu.uci.ics.jung.visualization.DefaultVisualizationModel

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scalafx.beans.property.BooleanProperty
import scalafx.scene.input.{ZoomEvent, ScrollEvent, MouseEvent}
import scalafx.scene.shape.{Rectangle}
import scalafx.scene.{Group, Node}
import scalafx.Includes._

object GraphFx {
  def defaultLayout[V,E](graph:Graph[V,E]) : AbstractNodeLayout[V] = {
    val layout = new JungSugiyama[V, E](graph)

    // run the layout algorithm
    val vm1 = new DefaultVisualizationModel(layout, new java.awt.Dimension(400, 400))

    new JungLayoutAdapter(layout)
  }

  def make[V, E](graph: Graph[V, E],
                 nodeVisualizer: AbstractNodeConfiguration[V] = new DefaultNodeConfiguration[V](),
                 edgeVisualizer: AbstractEdgeConfiguration[E] = new DefaultEdgeConfiguration[E](),
                 draggingEnabled: BooleanProperty): GraphFx[V, E] = {

    new GraphFx[V, E](graph, defaultLayout(graph), nodeVisualizer, edgeVisualizer, draggingEnabled)
  }
}


trait Trashable {
  val isTrashed = BooleanProperty(false)
}



class GraphFx[V, E](graph: Graph[V, E],
                    layout: AbstractNodeLayout[V],
                    nodeVisualizer: AbstractNodeConfiguration[V],
                    edgeVisualizer: AbstractEdgeConfiguration[E],
                    draggingEnabled: BooleanProperty) extends Group with Trashable {

  val backgroundRectangle = new Rectangle {  //Groups don't support much CSS
    styleClass.add("graph_bg")
  }

  val graphState = new GraphFxState
  var oldBoundsInParent = boundsInParent.value
  var lastNodeDragHorizontalSector = 0
  var lastNodeDragVerticalSector = 0
  var dragIsValid = false
  var zoomIsValid = false

  var previewDisabled = BooleanProperty(false)  //Only used by graphs in the preview section
  previewDisabled onChange { (a, b, c) =>
    backgroundRectangle.styleClass.clear()
    if (previewDisabled.value) {
      backgroundRectangle.styleClass.add("graph_trash")   //should change that later
    } else {
      backgroundRectangle.styleClass.add("graph_bg")
    }
  }

  graphState.disableChildInteraction()

  /*
      RENDER GRAPH
   */

  var nodeList : List[GraphNodeFx[V]] = Nil

  children = renderGraph

  private def renderGraph: Seq[Node] = {
    val ch = new ArrayBuffer[Node]()
    ch += backgroundRectangle
    val nodeMap = new mutable.HashMap[V, GraphNodeFx[V]]()
    val markers = new ArrayBuffer[Node]()

    // lay out the vertices
    foreach[V](graph.getVertices, v => {
      val n = new GraphNodeFx(v, nodeVisualizer, graphState,
                              {(h:Int, v:Int) => lastNodeDragHorizontalSector = h; lastNodeDragVerticalSector = v})
      val p = layout.position(v)
      nodeList = n :: nodeList

      n.layoutX = p.getX - n.width / 2
      n.layoutY = p.getY - n.height / 2

      nodeMap(v) = n
    })

    // draw the edges
    foreach[E](graph.getEdges, e => {
      val endpoints = graph.getEndpoints(e)
      val pStart = nodeMap(endpoints.getFirst)
      val pEnd = nodeMap(endpoints.getSecond)

      ch += new GraphEdgeFx[V,E](e, pStart, pEnd, edgeVisualizer)
    })

    // first edges, then nodes => nodes drawn on top of edges
    ch ++ nodeMap.values ++ markers
  }



  /*
      HANDLE ZOOM, SCROLL, AND DRAG
   */

  val dragDelta = new Delta(0, 0)

  def disableInteraction(): Unit = {
    graphState.disableChildInteraction()
    dragIsValid = false
    onZoom = {ev: ZoomEvent => }
    onZoomFinished = {ev: ZoomEvent => }
    onScroll = {ev: ScrollEvent => }
    onScrollFinished = {ev: ScrollEvent => }
    backgroundRectangle.onMousePressed = {ev: MouseEvent => }
    backgroundRectangle.onMouseDragged = {ev: MouseEvent => }
    backgroundRectangle.onMouseReleased = {ev: MouseEvent => }
  }

  def enableInteraction(): Unit = {
    graphState.enableChildInteraction()
    onZoomStarted = { ev : ZoomEvent =>
      if (draggingEnabled.value || dragIsValid) {
        draggingEnabled.set(false)
        zoomIsValid = true
        dragIsValid = false   //zoom should have priority
      }
    }
    onZoom = { ev : ZoomEvent =>
      if (zoomIsValid) {
        toFront()
        dragIsValid = false
        graphState.disableChildInteraction()
        lastNodeDragHorizontalSector = 0    //-1: Left half, 1: Right half, 0: Not dragged / center
        lastNodeDragVerticalSector = 0
        handleZoom(this)(ev)
        oldBoundsInParent = boundsInParent.value
        ev.consume()
      }
    }

    onZoomFinished = { ev: ZoomEvent =>
      draggingEnabled.set(true)
      zoomIsValid = false
      graphState.enableChildInteraction()
      ev.consume()
      popoversToFront()
    }

    onScroll = { ev: ScrollEvent =>
      toFront()
      dragIsValid = false
      graphState.disableChildInteraction()
      lastNodeDragHorizontalSector = 0
      lastNodeDragVerticalSector = 0
      handleScroll(this)(ev)
      updateStyle(Trash.coordsInTrash(ev.getX.getValue * scaleX.value + oldBoundsInParent.getMinX,
        ev.getY.getValue * scaleY.value + oldBoundsInParent.getMinY))
      oldBoundsInParent = boundsInParent.value
    }

    onScrollFinished = { ev : ScrollEvent =>
      graphState.enableChildInteraction()
      if (Trash.coordsInTrash(ev.getX.getValue*scaleX.value + boundsInParent.value.getMinX,
        ev.getY.getValue*scaleY.value + boundsInParent.value.getMinY)) {
        Trash.trash(this)
        for (node <- nodeList) {
          node.removePopover()
        }
        Trash.deactivate()
        isTrashed.set(true)
      }
      ev.consume()
      popoversToFront()
    }


    backgroundRectangle.onMousePressed = { ev: MouseEvent =>
      if (draggingEnabled.value) {
        draggingEnabled.set(false)
        toFront()
        if (graphState.nodeIsBeingDragged) {
          dragIsValid = false
        } else {
          dragIsValid = true
          dragDelta.x = layoutX.value - ev.sceneX / parent.value.scaleX.value
          dragDelta.y = layoutY.value - ev.sceneY / parent.value.scaleY.value
        }
        ev.consume()
      }
    }

    backgroundRectangle.onMouseDragged  = { ev: MouseEvent =>
      if (dragIsValid) {
        layoutX = ev.sceneX / parent.value.scaleX.value + dragDelta.x
        layoutY = ev.sceneY / parent.value.scaleY.value + dragDelta.y
        /*updateStyle(Trash.coordsInTrash(ev.getX.getValue * scaleX.value + oldBoundsInParent.getMinX,
          ev.getY.getValue * scaleY.value + oldBoundsInParent.getMinY)) */
        updateStyle(Trash.coordsInTrash(ev.sceneX, ev.sceneY))
        oldBoundsInParent = boundsInParent.value
      }
      ev.consume()
    }

    backgroundRectangle.onMouseReleased = { ev: MouseEvent =>
      /*if (Trash.coordsInTrash(ev.getX.getValue*scaleX.value + boundsInParent.value.getMinX,
        ev.getY.getValue*scaleY.value + boundsInParent.value.getMinY) && dragIsValid) {*/
      if (Trash.coordsInTrash(ev.sceneX, ev.sceneY) && dragIsValid) {
        Trash.trash(this)
        for (node <- nodeList) {
          node.removePopover()
        }
        Trash.deactivate()
        isTrashed.set(true)
      }
      draggingEnabled.set(true)
      dragIsValid = false
      ev.consume()
      popoversToFront()
    }
  }

  def popoversToFront(): Unit = {
    for (n <- nodeList) {
      n.popoverToFront()
    }
  }

  def updateStyle(overTrash : Boolean) = {
    if (overTrash) {
      backgroundRectangle.styleClass.clear()
      Trash.activate()
      //Trash.node.toFront()
      backgroundRectangle.styleClass.add("graph_trash")
    } else {
      backgroundRectangle.styleClass.clear()
      Trash.deactivate()
      backgroundRectangle.styleClass.add("graph_bg")
    }
  }

  /*
      HANDLE GROUP RESIZES
   */

  def resizeBackground(): Unit = {
    this.synchronized {
      backgroundRectangle.setX(boundsInLocal.value.getMinX + 10)
      backgroundRectangle.setY(boundsInLocal.value.getMinY + 10)
      if (lastNodeDragHorizontalSector > 0) {
        translateX = translateX.value + (oldBoundsInParent.getMinX - boundsInParent.value.getMinX)
      } else if (lastNodeDragHorizontalSector < 0) {
        translateX = translateX.value + (oldBoundsInParent.getMaxX - boundsInParent.value.getMaxX)
      }
      if (lastNodeDragVerticalSector > 0) {
        translateY = translateY.value + (oldBoundsInParent.getMinY - boundsInParent.value.getMinY)
      } else if (lastNodeDragVerticalSector < 0) {
        translateY = translateY.value + (oldBoundsInParent.getMaxY - boundsInParent.value.getMaxY)
      }
      oldBoundsInParent = boundsInParent.value
      backgroundRectangle.setHeight(math.max(boundsInLocal.value.getHeight, Configuration.graphMinSideLength) - 20)
      backgroundRectangle.setWidth(math.max(boundsInLocal.value.getWidth, Configuration.graphMinSideLength) - 20)
    }

  }
  boundsInLocal onChange resizeBackground
}

class GraphFxState {  //Usage: A reference can be passed to the nodes of the graph
  private var childInteractionEnabled_ = true
  private var nodeIsBeingDragged_ = false

  def childInteractionEnabled : Boolean = childInteractionEnabled_
  def enableChildInteraction() = { childInteractionEnabled_ = true }
  def disableChildInteraction() = { childInteractionEnabled_ = false }

  def nodeIsBeingDragged : Boolean = nodeIsBeingDragged_
  def nodeIsBeingDragged_=(v : Boolean) = {
    nodeIsBeingDragged_ = v
  }
}
