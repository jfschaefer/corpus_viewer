package de.jfschaefer.corpus_viewer


import java.io.FileInputStream
import java.util.Properties


object Configuration {
  val (displayScreen,
    jungHorizontalSpacing,
    jungVerticalSpacing,
    dragThreshold,
    minimalMouseClickDelay,
    trashWidth,
    trashHeight,
    touchSliderWidth,
    styleSheet,
    nodeMinWidth,
    graphMinSideLength,
    sizeOfPreviewSection,
    previewMinDrag,
    considerPreviewTrashArea
  ) = try {
    val props = new Properties()
    props.load(new FileInputStream("corpus_viewer.properties"))
    (
      props.getProperty("display_screen").toInt,
      props.getProperty("jung_horizontal_spacing").toInt,
      props.getProperty("jung_vertical_spacing").toInt,
      props.getProperty("drag_threshold").toDouble,
      props.getProperty("minimal_mouse_click_delay").toInt,
      props.getProperty("trash_width").toInt,
      props.getProperty("trash_height").toInt,
      props.getProperty("touch_slider_width").toInt,
      props.getProperty("style_sheet"),
      props.getProperty("node_min_width").toInt,
      props.getProperty("graph_min_side_length").toInt,
      props.getProperty("width_of_preview_section").toInt,
      props.getProperty("preview_min_drag").toInt,
      props.getProperty("consider_preview_area_trash_area").toBoolean
    )
  } catch { case exc: Exception =>
    exc.printStackTrace()
    sys.exit(1)
  }
}
