package de.jfschaefer.corpus_viewer

import scala.collection.mutable.ArrayBuffer
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.{Node, Group}
import scalafx.scene.shape.Rectangle

object Trash {
  val node = new Group {
    val iv = new ImageView {
      image = new Image("file://" + System.getProperty("user.dir") + "/icons/trash.png")
    }
    iv.setFitWidth(Configuration.trashWidth)
    iv.setFitHeight(Configuration.trashHeight)
    //children = new ArrayBuffer[Node]()
    children = List[Node](iv)
  }

  deactivate()


  //Default trash method. This is supposed to be overridden.
  var trash : Node => Unit = {n : Node => println("Trashing " + n.toString)}

  def getNode : Node = node

  def coordsInTrash(x : Double, y : Double) : Boolean = {
    //println("TrashCheck: " + x + " | " + y)
    (Configuration.considerPreviewTrashArea && x < Configuration.sizeOfPreviewSection) ||
      (x > node.getBoundsInParent.getMinX) && (x < node.getBoundsInParent.getMaxX) &&
      (y > node.getBoundsInParent.getMinY) && (y < node.getBoundsInParent.getMaxY)
  }

  def activate() = {
    node.styleClass.clear()
    node.styleClass.add("graph_trash")   //Need to change this at some point
  }

  def deactivate() = {
    node.styleClass.clear()
    node.styleClass.add("graph_bg")   //Need to change this at some point
  }

  def placeTrashBottomLeft() = {
    node.setLayoutX(node.getScene.getWindow.getWidth - Configuration.trashWidth - 30)
    node.setLayoutY(node.getScene.getWindow.getHeight - Configuration.trashHeight - 30)
  }
}
